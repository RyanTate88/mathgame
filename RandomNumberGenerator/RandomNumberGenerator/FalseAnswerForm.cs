﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RandomNumberGenerator
{
    public partial class FalseAnswerForm : Form
    {
        public FalseAnswerForm()
        {
            InitializeComponent();
        }

        public FalseAnswerForm(String correctAnswer)
        {
            InitializeComponent();
            correctAnswerLabel.Text = correctAnswer;
        }
    }
}
