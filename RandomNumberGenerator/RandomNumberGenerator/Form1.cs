﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;


namespace RandomNumberGenerator
{
    public partial class startForm : Form
    {
        private SoundPlayer soundPlayer;

        public startForm()
        {
            InitializeComponent();
            soundPlayer = new SoundPlayer(RandomNumberGenerator.Properties.Resources.SpongebobBackground);
            soundPlayer.PlayLooping();
        }

        private void startIntegerButton_Click(object sender, EventArgs e)
        {
            IntegerForm intForm = new IntegerForm();
            intForm.Show();

            soundPlayer.Stop();
        }

    }
}
