﻿namespace RandomNumberGenerator
{
    partial class IntegerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IntegerForm));
            this.secondNumberLabel = new System.Windows.Forms.Label();
            this.operatorLabel = new System.Windows.Forms.Label();
            this.firstNumberLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.percentLabel = new System.Windows.Forms.Label();
            this.correctLabel = new System.Windows.Forms.Label();
            this.totalLabel = new System.Windows.Forms.Label();
            this.incorrectLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.answerButton = new System.Windows.Forms.Button();
            this.answerTextBox = new System.Windows.Forms.TextBox();
            this.equalSignLabel = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.settingsGroupBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.resetButton = new System.Windows.Forms.Button();
            this.divisionCheckBox = new System.Windows.Forms.CheckBox();
            this.multiplicationCheckBox = new System.Windows.Forms.CheckBox();
            this.subtractionCheckBox = new System.Windows.Forms.CheckBox();
            this.additionCheckBox = new System.Windows.Forms.CheckBox();
            this.startButton = new System.Windows.Forms.Button();
            this.highNumberTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numberOfQsTextBox = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.settingsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // secondNumberLabel
            // 
            this.secondNumberLabel.AutoSize = true;
            this.secondNumberLabel.Font = new System.Drawing.Font("Bauhaus 93", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.secondNumberLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.secondNumberLabel.Location = new System.Drawing.Point(195, 165);
            this.secondNumberLabel.Name = "secondNumberLabel";
            this.secondNumberLabel.Size = new System.Drawing.Size(79, 54);
            this.secondNumberLabel.TabIndex = 11;
            this.secondNumberLabel.Text = "10";
            this.secondNumberLabel.Visible = false;
            // 
            // operatorLabel
            // 
            this.operatorLabel.AutoSize = true;
            this.operatorLabel.Font = new System.Drawing.Font("Bauhaus 93", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.operatorLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.operatorLabel.Location = new System.Drawing.Point(141, 165);
            this.operatorLabel.Name = "operatorLabel";
            this.operatorLabel.Size = new System.Drawing.Size(48, 54);
            this.operatorLabel.TabIndex = 10;
            this.operatorLabel.Text = "*";
            this.operatorLabel.Visible = false;
            // 
            // firstNumberLabel
            // 
            this.firstNumberLabel.AutoSize = true;
            this.firstNumberLabel.BackColor = System.Drawing.Color.Transparent;
            this.firstNumberLabel.Font = new System.Drawing.Font("Bauhaus 93", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.firstNumberLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.firstNumberLabel.Location = new System.Drawing.Point(56, 165);
            this.firstNumberLabel.Name = "firstNumberLabel";
            this.firstNumberLabel.Size = new System.Drawing.Size(79, 54);
            this.firstNumberLabel.TabIndex = 9;
            this.firstNumberLabel.Text = "10";
            this.firstNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.firstNumberLabel.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 20);
            this.label6.TabIndex = 1;
            this.label6.Text = "Correct:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Incorrect:";
            // 
            // percentLabel
            // 
            this.percentLabel.AutoSize = true;
            this.percentLabel.Location = new System.Drawing.Point(89, 134);
            this.percentLabel.Name = "percentLabel";
            this.percentLabel.Size = new System.Drawing.Size(0, 20);
            this.percentLabel.TabIndex = 8;
            // 
            // correctLabel
            // 
            this.correctLabel.AutoSize = true;
            this.correctLabel.Location = new System.Drawing.Point(89, 37);
            this.correctLabel.Name = "correctLabel";
            this.correctLabel.Size = new System.Drawing.Size(0, 20);
            this.correctLabel.TabIndex = 3;
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Location = new System.Drawing.Point(89, 103);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(0, 20);
            this.totalLabel.TabIndex = 6;
            // 
            // incorrectLabel
            // 
            this.incorrectLabel.AutoSize = true;
            this.incorrectLabel.Location = new System.Drawing.Point(89, 71);
            this.incorrectLabel.Name = "incorrectLabel";
            this.incorrectLabel.Size = new System.Drawing.Size(0, 20);
            this.incorrectLabel.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 5;
            this.label7.Text = "Total:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Percent:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.incorrectLabel);
            this.groupBox1.Controls.Add(this.totalLabel);
            this.groupBox1.Controls.Add(this.correctLabel);
            this.groupBox1.Controls.Add(this.percentLabel);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(410, 418);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 169);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Results";
            // 
            // answerButton
            // 
            this.answerButton.Enabled = false;
            this.answerButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerButton.Location = new System.Drawing.Point(499, 153);
            this.answerButton.Name = "answerButton";
            this.answerButton.Size = new System.Drawing.Size(168, 78);
            this.answerButton.TabIndex = 16;
            this.answerButton.Text = "Answer";
            this.answerButton.UseVisualStyleBackColor = true;
            this.answerButton.Visible = false;
            this.answerButton.Click += new System.EventHandler(this.answerButton_Click);
            // 
            // answerTextBox
            // 
            this.answerTextBox.Font = new System.Drawing.Font("Bauhaus 93", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.answerTextBox.Location = new System.Drawing.Point(352, 153);
            this.answerTextBox.Name = "answerTextBox";
            this.answerTextBox.Size = new System.Drawing.Size(107, 78);
            this.answerTextBox.TabIndex = 13;
            this.answerTextBox.Visible = false;
            // 
            // equalSignLabel
            // 
            this.equalSignLabel.AutoSize = true;
            this.equalSignLabel.Font = new System.Drawing.Font("Bauhaus 93", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equalSignLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.equalSignLabel.Location = new System.Drawing.Point(280, 165);
            this.equalSignLabel.Name = "equalSignLabel";
            this.equalSignLabel.Size = new System.Drawing.Size(60, 54);
            this.equalSignLabel.TabIndex = 12;
            this.equalSignLabel.Text = "=";
            this.equalSignLabel.Visible = false;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Font = new System.Drawing.Font("Arial Rounded MT Bold", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.titleLabel.Location = new System.Drawing.Point(73, 9);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(619, 110);
            this.titleLabel.TabIndex = 17;
            this.titleLabel.Text = "Can you help Spongebob \r\nsolve math problems?";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // settingsGroupBox
            // 
            this.settingsGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.settingsGroupBox.Controls.Add(this.label4);
            this.settingsGroupBox.Controls.Add(this.resetButton);
            this.settingsGroupBox.Controls.Add(this.divisionCheckBox);
            this.settingsGroupBox.Controls.Add(this.multiplicationCheckBox);
            this.settingsGroupBox.Controls.Add(this.subtractionCheckBox);
            this.settingsGroupBox.Controls.Add(this.additionCheckBox);
            this.settingsGroupBox.Controls.Add(this.startButton);
            this.settingsGroupBox.Controls.Add(this.highNumberTextBox);
            this.settingsGroupBox.Controls.Add(this.label3);
            this.settingsGroupBox.Controls.Add(this.label1);
            this.settingsGroupBox.Controls.Add(this.numberOfQsTextBox);
            this.settingsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settingsGroupBox.Location = new System.Drawing.Point(12, 280);
            this.settingsGroupBox.Name = "settingsGroupBox";
            this.settingsGroupBox.Size = new System.Drawing.Size(381, 307);
            this.settingsGroupBox.TabIndex = 9;
            this.settingsGroupBox.TabStop = false;
            this.settingsGroupBox.Text = "Only for teacher";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Operators";
            // 
            // resetButton
            // 
            this.resetButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetButton.Location = new System.Drawing.Point(277, 248);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(98, 53);
            this.resetButton.TabIndex = 11;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // divisionCheckBox
            // 
            this.divisionCheckBox.AutoSize = true;
            this.divisionCheckBox.Location = new System.Drawing.Point(125, 186);
            this.divisionCheckBox.Name = "divisionCheckBox";
            this.divisionCheckBox.Size = new System.Drawing.Size(82, 24);
            this.divisionCheckBox.TabIndex = 10;
            this.divisionCheckBox.Text = "Division";
            this.divisionCheckBox.UseVisualStyleBackColor = true;
            this.divisionCheckBox.CheckedChanged += new System.EventHandler(this.divisionCheckBox_CheckedChanged);
            // 
            // multiplicationCheckBox
            // 
            this.multiplicationCheckBox.AutoSize = true;
            this.multiplicationCheckBox.Location = new System.Drawing.Point(5, 186);
            this.multiplicationCheckBox.Name = "multiplicationCheckBox";
            this.multiplicationCheckBox.Size = new System.Drawing.Size(119, 24);
            this.multiplicationCheckBox.TabIndex = 9;
            this.multiplicationCheckBox.Text = "Multiplication";
            this.multiplicationCheckBox.UseVisualStyleBackColor = true;
            this.multiplicationCheckBox.CheckedChanged += new System.EventHandler(this.multiplicationCheckBox_CheckedChanged);
            // 
            // subtractionCheckBox
            // 
            this.subtractionCheckBox.AutoSize = true;
            this.subtractionCheckBox.Location = new System.Drawing.Point(97, 156);
            this.subtractionCheckBox.Name = "subtractionCheckBox";
            this.subtractionCheckBox.Size = new System.Drawing.Size(110, 24);
            this.subtractionCheckBox.TabIndex = 8;
            this.subtractionCheckBox.Text = "Subtraction";
            this.subtractionCheckBox.UseVisualStyleBackColor = true;
            this.subtractionCheckBox.CheckedChanged += new System.EventHandler(this.subtractionCheckBox_CheckedChanged);
            // 
            // additionCheckBox
            // 
            this.additionCheckBox.AutoSize = true;
            this.additionCheckBox.Location = new System.Drawing.Point(5, 156);
            this.additionCheckBox.Name = "additionCheckBox";
            this.additionCheckBox.Size = new System.Drawing.Size(86, 24);
            this.additionCheckBox.TabIndex = 7;
            this.additionCheckBox.Text = "Addition";
            this.additionCheckBox.UseVisualStyleBackColor = true;
            this.additionCheckBox.CheckedChanged += new System.EventHandler(this.additionCheckBox_CheckedChanged);
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(6, 248);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(98, 53);
            this.startButton.TabIndex = 5;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // highNumberTextBox
            // 
            this.highNumberTextBox.Location = new System.Drawing.Point(176, 83);
            this.highNumberTextBox.Name = "highNumberTextBox";
            this.highNumberTextBox.Size = new System.Drawing.Size(114, 26);
            this.highNumberTextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(42, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Highest Number:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of Questions:";
            // 
            // numberOfQsTextBox
            // 
            this.numberOfQsTextBox.Location = new System.Drawing.Point(176, 34);
            this.numberOfQsTextBox.Name = "numberOfQsTextBox";
            this.numberOfQsTextBox.Size = new System.Drawing.Size(114, 26);
            this.numberOfQsTextBox.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(611, 189);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(476, 408);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // IntegerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(232)))), ((int)(((byte)(46)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1075, 599);
            this.Controls.Add(this.settingsGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.answerButton);
            this.Controls.Add(this.answerTextBox);
            this.Controls.Add(this.equalSignLabel);
            this.Controls.Add(this.secondNumberLabel);
            this.Controls.Add(this.operatorLabel);
            this.Controls.Add(this.firstNumberLabel);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IntegerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IntegerForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.settingsGroupBox.ResumeLayout(false);
            this.settingsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label secondNumberLabel;
        private System.Windows.Forms.Label operatorLabel;
        private System.Windows.Forms.Label firstNumberLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label percentLabel;
        private System.Windows.Forms.Label correctLabel;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.Label incorrectLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button answerButton;
        private System.Windows.Forms.TextBox answerTextBox;
        private System.Windows.Forms.Label equalSignLabel;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.GroupBox settingsGroupBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.CheckBox divisionCheckBox;
        private System.Windows.Forms.CheckBox multiplicationCheckBox;
        private System.Windows.Forms.CheckBox subtractionCheckBox;
        private System.Windows.Forms.CheckBox additionCheckBox;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.TextBox highNumberTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox numberOfQsTextBox;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}