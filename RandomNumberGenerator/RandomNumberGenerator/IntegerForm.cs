﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace RandomNumberGenerator
{
    public partial class IntegerForm : Form
    {
        // declare vars
        private Random r;

        private int firstNumber;
        private int secondNumber;
        private int correctAnswer;
        private string correctAnsString;
        private double doubleCorrectAnswer;

        private double percent;

        private int count;
        private int correct;
        private int incorrect;

        private int questions;
        private int highestNumber;

        private List<char> operatorList;

        private int isChecked;
        private bool isDivision;
        
        public IntegerForm()
        {
            InitializeComponent();
            initVars(); // init vars in constructor
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            // declare local vars
            int questions = 0;
            int highestNumber = 0;

            // Only true if both textboxes contain integers
            if (int.TryParse(numberOfQsTextBox.Text, out questions) && int.TryParse(highNumberTextBox.Text, out highestNumber))
            {
                // makes sure that atleast 1 checkbox has been checked
                if (isChecked > 0)
                {
                    setVisibility(true); // set equation to visible

                    this.questions = questions; // assign int from textbox, only if its an int
                    this.highestNumber = highestNumber + 1; // add one to the number, because random uses n-1 as highest number

                    getNextNumbers(); // see method for doc
                    resetAndFocusAnswerTextBox(); // see method for doc

                    answerButton.Enabled = true; //enable answer button once program starts
                    settingsGroupBox.Enabled = false; // disable group box, as child should not interct with it
                }
                else
                {
                    // user did not check a box
                    MessageBox.Show("Please check at least one math operator.");
                }

            }
            else
            {
                // user did not enter number
                MessageBox.Show("Please fill out all textboxes.");
            }
        }

        private void answerButton_Click(object sender, EventArgs e)
        {
            // check if user input is an int, when the equation doesn't contain a division symbol
            if (isDivision == false)
            {
                int response = 0;
                if (int.TryParse(answerTextBox.Text, out response))
                {
                    // makes sure that only we don't have infinite q's and only amount teacher selected
                    if (count < questions)
                    {
                        checkIntAnswer(response);
                        getNextNumbers();
                        resetAndFocusAnswerTextBox();

                    }
                    else
                    {
                        checkIntAnswer(response);
                        answerClickElse();
                    }
                }
            }
            else
            {
                double response = 0;
                if (double.TryParse(answerTextBox.Text, out response))
                {

                    if (count < questions)
                    {
                        checkDoubleAnswer(response);
                        getNextNumbers();
                        resetAndFocusAnswerTextBox();

                    }
                    else
                    {
                        checkDoubleAnswer(response);
                        answerClickElse();
                        
                    }
                }
            }
      
        }

        // This method displays results among other things. The reason this was created, was to prevent
        // code being copied between int and double tryParse above.  
        private void answerClickElse()
        {

            SoundPlayer soundPlayer = new SoundPlayer(RandomNumberGenerator.Properties.Resources.weDidAlright);
            soundPlayer.Play();

            percent = (double)correct / count;

            //executed one user presses the answer button on the last qustion
            correctLabel.Text = correct.ToString();
            incorrectLabel.Text = incorrect.ToString();
            totalLabel.Text = questions.ToString();
            percentLabel.Text = percent.ToString("p");
            settingsGroupBox.Enabled = true;
            startButton.Enabled = false;
            answerButton.Enabled = false;
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            // resets all of the items (vars check boxes, text boxes etc.)
            titleLabel.Text = "Can you help Spongebob\n solve math problems?";
            answerTextBox.Text = "";
            numberOfQsTextBox.Text = "";
            highNumberTextBox.Text = "";
            additionCheckBox.Checked = false;
            subtractionCheckBox.Checked = false;
            divisionCheckBox.Checked = false;
            multiplicationCheckBox.Checked = false;
            answerButton.Enabled = false;
            settingsGroupBox.Enabled = true;
            startButton.Enabled = true;
            correctLabel.Text = "";
            incorrectLabel.Text = "";
            totalLabel.Text = "";
            percentLabel.Text = "";

            initVars();

        }

        // This method executes everytime the checkbox is either checked/unchecked
        private void additionCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (additionCheckBox.Checked)
            {
                // if the checkbox is checked, add the opertor to a list, which will
                // later be used in an equation.
                operatorList.Add('+');

                // add one to the flag, to make sure that a checkbox is checked.
                // when starting the game, we need at least 1 operator/checked check box.
                isChecked++;
            }
            else
            {
                // List<char>.Remove() removes the first occurrence of a specific object from 
                // the List<char>, which is all we need, because the list doesn't contain any 
                // duplicate chars.
                //
                // removes the operator from the list. 
                operatorList.Remove('+');

                // decrement isChecked flag, to make sure that the game
                // cannot start, unless a checkbox is checked
                isChecked--;
            }
        }

        private void subtractionCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (subtractionCheckBox.Checked)
            {
                operatorList.Add('-');
                isChecked++;
            }
            else
            {
                operatorList.Remove('-');
                isChecked--;
            }
        }

        private void multiplicationCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (multiplicationCheckBox.Checked)
            {
                operatorList.Add('*');
                isChecked++;
            }
            else
            {
                operatorList.Remove('*');
                isChecked--;
            }
        }

        private void divisionCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (divisionCheckBox.Checked)
            {
                operatorList.Add('/');
                isChecked++;
            }
            else
            {
                operatorList.Remove('/');
                isChecked--;
            }
        }

        // This method initializes all variables
        private void initVars()
        {

            // create new random object
            r = new Random();

            //first number in math equation
            firstNumber = 0;

            //second number in math equation
            secondNumber = 0;

            // keeps track of number of questions
            count = 0;

            // the number of questions that will be asked
            questions = 0;

            // the highest number that can randomly appear
            highestNumber = 0;

            // total # of correct q's
            correct = 0;

            // total # of incorrect q's
            incorrect = 0;

            // list to keep track of all operators user added to the game
            operatorList = new List<char>();

            // the correct answer of the current problem
            correctAnswer = 0;

            // the whole equation for getting correct answer written out
            correctAnsString = "";

            //flag to make sure at least 1 checkbox is checked
            isChecked = 0;

            // the percentage of q's the user got right
            percent = 0;

            // flag to make sure that we parse for int if equation does not contain division and 
            // parse for double if equation contains division
            isDivision = false;

            // the answer of the current problem, if equation contains a division
            doubleCorrectAnswer = 0;

        }

        // This methods shows or hides the equation
        private void setVisibility(bool visibility)
        {
            firstNumberLabel.Visible = visibility;
            operatorLabel.Visible = visibility;
            secondNumberLabel.Visible = visibility;
            equalSignLabel.Visible = visibility;
            answerTextBox.Visible = visibility;
            answerButton.Visible = visibility;
        }

        // This methids resets the answer textbox and gives it focus
        private void resetAndFocusAnswerTextBox()
        {
            answerTextBox.Text = "";
            answerTextBox.Focus();
        }

        /*
         * This method generates a set of random numbers and also generates a random
         * operator of either +-/*
         */
        private void getNextNumbers()
        {
            count++; // add one to count

            // changes the title from "Can you help..." to the current qestion user is on
            titleLabel.Text = "Question #" + count;

            // generates a random number, with the high value beeing the size of the list.
            // This variable is used to retrive an item from the operator list.
            int randomOperator = r.Next(operatorList.Count);

            // gets an item from the list at a position that was randomly generated.
            // This will return a randomly selected oprator. 
            char mathOperator = operatorList.ElementAt(randomOperator);

            // if math operator is a division, set flag to true, so that the user answer can be parsed as a double.
            if (mathOperator == '/')
            {
                isDivision = true;
            }
            else
            {
                isDivision = false;
            }
            firstNumber = r.Next(highestNumber); // genrates a new random number between 0 and the highest number user selected
            secondNumber = r.Next(highestNumber); // genrates a new random number
         
            // Here a switch statement is used, because you cannot insert an operator, of type char 
            // into an equation, so all 4 possible equatons are written out.
            switch (mathOperator)
            {
                case '+':
                    // if the random item retrived from the list was a +, then add numbers
                    correctAnswer = firstNumber + secondNumber;
                    break;
                case '-':
                    // Here a new random number is generated, for the second number, 
                    // with the high, being the first number. This is done, because
                    // if the second number is greater than the first number, 
                    // when using subtraction, we would get a negative number. 
                    // First graders do not know negatives yet.
                    secondNumber = r.Next(0, firstNumber);

                    correctAnswer = firstNumber - secondNumber;
                    break;
                case '*':
                    correctAnswer = firstNumber * secondNumber;
                    break;
                case '/':

                    // Create new number between 1 and the highest number,
                    // because firtNumber is used as highest number, when generating second number.
                    // min cannot be greater than max.
                    firstNumber = r.Next(1, highestNumber);

                    /* Here a second number is generated,
                     between 1 and the first number. This is done to make sure
                     that the second number will always be greater than 0.
                     In math you cannot divide by 0.
                     It also ensures that kids won't be asked a question
                     where the fraction is less than 1. */
                    secondNumber = r.Next(1, firstNumber);

                    // convert first number to a double, because dividing int's 
                    // won't produce a double
                    //
                    // new variable is created, because if we put "firstNUmber / secondNumber"
                    // into math.Round, it would be ambiguous, between if it's a deciaml or double. 
                    double unroundedAnswer = (double)firstNumber / secondNumber; 

                    doubleCorrectAnswer = Math.Round(unroundedAnswer, 1); // round answer to 2 decimal places
                    
                    break;
            }

            firstNumberLabel.Text = firstNumber.ToString(); // dispaly first number to user
            secondNumberLabel.Text = secondNumber.ToString(); // dispaly second number to user
            operatorLabel.Text = mathOperator.ToString(); //  dispaly operator to user

            if (isDivision)
            {
                // equation written out using the double correct answer, because euqation used division
                correctAnsString = firstNumber + " " + mathOperator + " "
               + secondNumber + " = " + doubleCorrectAnswer; 
            }
            else
            {
                correctAnsString = firstNumber + " " + mathOperator + " "
                + secondNumber + " = " + correctAnswer; // equation written out
            }
            
        }

        // This method checks the answer for *+-
        private void checkIntAnswer(int response)
        {
            // makes sure user response is correct
            if (response == correctAnswer)
            {
                CorrectAnswerForm correctAnsForm = new CorrectAnswerForm();
                correctAnsForm.ShowDialog();

                correct++; // add 1 to correct var
            }
            else
            {
                FalseAnswerForm falseAnsForm = new FalseAnswerForm(correctAnsString);
                falseAnsForm.ShowDialog();

                incorrect++; // add 1 to incorrect var
            }
        }

        // This method checks the answer for /
        private void checkDoubleAnswer(double response)
        {
            // makes sure user response is correct
            if (response == doubleCorrectAnswer)
            {
                CorrectAnswerForm correctAnsForm = new CorrectAnswerForm();
                correctAnsForm.ShowDialog();

                correct++; // add 1 to correct var
            }
            else
            {
                FalseAnswerForm falseAnsForm = new FalseAnswerForm(correctAnsString);
                falseAnsForm.ShowDialog();

                incorrect++; // add 1 to incorrect var
            }
        }
  
    }
}
