# Goals #

To create a program for an elementary math teacher. The program should help students learn math problems. The student has the to decide if they
want to work on addition, subtraction, multiplication, division, or any combination. The student will be able to designate the highest whole number
they could work with and the number of problems they will solve. Once attempting to solve a problem the student will receive immediate feedback.
After completing all the problems, the student will understand how many problems they got right. The student will then be able to change the
settings in the program and run the process all over again.

# Implementation #

The key concept for making this project successful was designing a user-friendly interface. Using object-oriented principles, the program was
created entirely in C#. The first major part of the program is the start button. The start button checks if user enters valid values for the number
of questions and also retrieves the highest whole number entered. It also discerns which operators have been selected to be used during the
operation of the program. After these checks, the beginning controls area is disabled, random numbers are generated for the first math problem, the
selected operator symbols are put into a collection list, the math problem area is enabled, and the students can start working on math problems. 

The second major part is the answer button. This is where the student is answering the math problems and receiving feedback. The answer first
starts by deciding if division is involved, because dividing an integer by a double in results in incorrect values. Then, it checks if the problem
has gone over the number questions they wanted to try. Next, the answer is checked to see if it correct. Finally, the program sets up new random
numbers and operator symbol.

Each form has its own class with unique methods.

# How to use it #
 
You start the program with a landing form and click "Help Spongebob Solve Integers" button to start the program. Then, you enter in the number of
question you would like to do and the highest whole number you could use. Next, you select any operator you wish to use and click start. You then
go to the math problem section and enter your guess and press answer. If you get the answer correct, you get a correct answer form and can click
okay to go back to working the problem. If you get the answer wrong, you get an incorrect answer form and can click okay to go back to working the
problems. Once you are done, you get a summary of the results in the results area. You can then enter in the number of questions, highest whole
number, and operators again.